package com.news.common.model;

import com.news.common.model.enumeration.Headline;
import lombok.Data;

import java.util.List;

/**
 * @author Ali Yeganeh
 * @since 09/07/2021
 */
@Data
public class News implements Comparable<News> {

    private List<Headline> headlineItem;
    private Integer priority;

    @Override
    public int compareTo(News news) {
        return this.getPriority().compareTo(news.getPriority());
    }
}