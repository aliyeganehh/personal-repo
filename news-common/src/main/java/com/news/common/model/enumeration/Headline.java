package com.news.common.model.enumeration;

/**
 * @author Ali Yeganeh
 * @since 09/07/2021
 */
public enum Headline {
    UP,
    DOWN,
    RISE,
    FALL,
    GOOD,
    BAD,
    SUCCESS,
    FAILURE,
    HIGH,
    LOW,
    ÜBER,
    UNTER;
}