package com.news.analysis.service;

import com.news.common.model.News;

/**
 * @author Ali Yeganeh
 * @since 09/08/2021
 */
public interface AnalysisNewsService {
    void analysisNewsService(News news);
}