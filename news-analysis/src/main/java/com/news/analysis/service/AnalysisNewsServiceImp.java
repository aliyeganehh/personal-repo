package com.news.analysis.service;

import com.news.common.model.News;
import com.news.common.model.enumeration.Headline;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Ali Yeganeh
 * @since 09/06/2021
 */
@Slf4j
@RequiredArgsConstructor
@Service
public class AnalysisNewsServiceImp implements AnalysisNewsService {

    private final List<News> positiveNews = new ArrayList<>();
    private final List<Headline> positiveHeadline;

    @Override
    public void analysisNewsService(News news) {
        List<Headline> headlineItem = news.getHeadlineItem();
        List<Headline> positiveHeadlineItem = headlineItem.stream().filter(positiveHeadline::contains).collect(Collectors.toList());
        if (((headlineItem.size() == 4 || headlineItem.size() == 5) && positiveHeadlineItem.size() == 3) ||
                (headlineItem.size() == 3 && positiveHeadlineItem.size() == 2)) {
            news.setHeadlineItem(positiveHeadlineItem);
            positiveNews.add(news);
        }
    }

    @Scheduled(initialDelay = 10_000, fixedRate = 10_000)
    public void releaseNews() {
        log.info("\nPositive news:\n{} \nCount of positive news: {}\n", positiveNews, positiveNews.size());

        Collections.sort(positiveNews);
        List<News> uniqueHighPriorityNews = positiveNews.stream()
                .peek(news -> news.setHeadlineItem(new ArrayList<>(new HashSet<>(news.getHeadlineItem()))))
                .limit(3)
                .collect(Collectors.toList());
        log.info("\nHighest priority of news: {}\n", uniqueHighPriorityNews);

        positiveNews.clear();
    }

}