package com.news.analysis.controller;

import com.news.analysis.service.AnalysisNewsService;
import com.news.common.model.News;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Ali Yeganeh
 * @since 09/07/2021
 */
@RestController
@RequiredArgsConstructor
public class AnalysisNewsController {

    private final AnalysisNewsService analysisNewsService;

    @PostMapping(value = "/news", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<HttpStatus> requestBody(@RequestBody News news) {
        analysisNewsService.analysisNewsService(news);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}