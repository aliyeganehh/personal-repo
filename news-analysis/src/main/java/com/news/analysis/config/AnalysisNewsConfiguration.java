package com.news.analysis.config;

import com.news.common.model.enumeration.Headline;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Arrays;
import java.util.List;

import static com.news.common.model.enumeration.Headline.*;

/**
 * @author Ali Yeganeh
 * @since 09/06/2021
 */
@Configuration
public class AnalysisNewsConfiguration {

    @Bean
    public List<Headline> positiveHeadline() {
        return Arrays.asList(UP, RISE, GOOD, SUCCESS, HIGH, ÜBER);
    }
}