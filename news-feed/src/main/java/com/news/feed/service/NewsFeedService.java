package com.news.feed.service;

/**
 * @author Ali Yeganeh
 * @since 09/08/2021
 */
public interface NewsFeedService {
    void releaseNews();
}