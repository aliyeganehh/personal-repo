package com.news.feed.service;

import com.news.common.model.News;
import com.news.feed.util.NewsFeedUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestTemplate;

/**
 * @author Ali Yeganeh
 * @since 09/06/2021
 */
@Slf4j
@Service
public class NewsFeedServiceImpl implements NewsFeedService {

    private static final String ANALYSIS_NEWS = "/news";
    private final RestTemplate restTemplate;

    public NewsFeedServiceImpl(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    @Override
    @Scheduled(fixedRateString = "${feed.release-news.period-time}")
    public void releaseNews() {
        News news = new News();
        news.setHeadlineItem(NewsFeedUtil.generateHeadline());
        news.setPriority(NewsFeedUtil.generatePrioritizedRandomNumber());

        try {
            restTemplate.postForEntity(restTemplate.getUriTemplateHandler().expand(ANALYSIS_NEWS), news, News.class);
        } catch (ResourceAccessException e) {
            log.error(e.getMessage());
        }
    }

}