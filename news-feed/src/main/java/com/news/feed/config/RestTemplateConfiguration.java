package com.news.feed.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.web.ServerProperties;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

import java.time.Duration;

/**
 * @author Ali Yeganeh
 * @since 09/06/2021
 */
@Configuration
public class RestTemplateConfiguration {

    @Value("${analysis.service.port}")
    private int analysisServicePort;

    @Bean
    public RestTemplate localRestTemplate(RestTemplateBuilder builder, ServerProperties serverProperties) {
        return builder.rootUri("http://" + serverProperties.getAddress().getHostAddress() + ":" + analysisServicePort)
                .setReadTimeout(Duration.ofMillis(60000))
                .setConnectTimeout(Duration.ofMillis(60000))
                .build();
    }
}