package com.news.feed.util;

import com.news.common.model.enumeration.Headline;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * @author Ali Yeganeh
 * @since 09/07/2021
 */
public final class NewsFeedUtil {

    private NewsFeedUtil() {
    }

    public static List<Headline> generateHeadline() {
        List<Headline> headlines = new ArrayList<>();
        int wordCount = generateRandomNumber(3, 6);
        for (int i = 0; i < wordCount; i++) {
            headlines.add(Headline.values()[generateRandomNumber(0, Headline.values().length)]);
        }
        return headlines;
    }

    public static int generatePrioritizedRandomNumber() {
        List<Integer> list = new ArrayList<>();
        int priorityNumber = -1;
        for (int i = 0; i < 10; i++) {
            priorityNumber++;
            for (int j = i; j < 10; j++) {
                list.add(priorityNumber);
            }
        }
        return list.get(generateRandomNumber(0, list.size()));
    }

    private static int generateRandomNumber(int minInclusive, int maxExclusive) {
        return new Random().nextInt(maxExclusive - minInclusive) + minInclusive;
    }

}